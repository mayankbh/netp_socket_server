import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class RegistrationHelperThread extends Thread {
	Socket clientSocket;
	ConcurrentHashMap<String,SongEntry> songMap;
	String clientIP;
	String messageString;
	
	public RegistrationHelperThread(Socket cliSocket,ConcurrentHashMap<String,SongEntry> sMap){
		clientSocket = cliSocket;
		songMap = sMap;
	}
	
	public void run(){
		
		clientIP = clientSocket.getRemoteSocketAddress().toString();
		
		messageString = readFromSocket(clientSocket);
		
		parseAndInsert(messageString,clientIP,songMap);
		
		writeResponseToSocket(clientSocket,true);
		
		closeSocket(clientSocket);
			
	}
	
	public void insertIntoMap(JSONObject song,String clientIP){
		
		String songTitle = (String) song.get(Constants.jsonTitleTag);
		String artistName = (String) song.get(Constants.jsonArtistTag);
		String key = songTitle.toLowerCase()+"_"+artistName.toLowerCase();
		int cIndex = clientIP.indexOf(':');
		String clIP = clientIP.substring(1, cIndex);
		if(songMap.containsKey(key)){
			SongEntry sEntry = songMap.get(key);
			if(!SongEntry.containsIP(sEntry, clIP))
				SongEntry.addIpToEntry(sEntry,clIP);
		} else {
			SongEntry sEntry = new SongEntry(songTitle, artistName, clIP);
			songMap.put(key, sEntry);
		}
		
	}
	
	public void parseAndInsert(String messageString,String clientIP,ConcurrentHashMap<String,SongEntry> songMap){
		try {
			JSONParser jsonParser = new JSONParser();
			JSONArray songArray = (JSONArray) jsonParser.parse(messageString);
			@SuppressWarnings("rawtypes")
			Iterator itr = songArray.iterator();
			while(itr.hasNext()){
				JSONObject song = (JSONObject) itr.next();
				insertIntoMap(song,clientIP);
			}
			print("New client successfully registered!");
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void writeResponseToSocket(Socket clientSocket,boolean value){
		JSONObject obj = new JSONObject();
		obj.put("Response", new Boolean(value));
		PrintWriter out = null;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		out.println(obj);
	}
	
	public String readFromSocket(Socket clientSocket){
		String inputLine = "";
		String messageString = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while ((inputLine = in.readLine()) != null) {
				messageString = messageString + inputLine;
		    }
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return messageString;
	}
	
	public void closeSocket(Socket clientSocket){
		try {
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void print(String str){
		System.out.println(str);
	}
}
