import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class PeerListHelperThread extends Thread {
	Socket clientSocket;
	ConcurrentHashMap<String,SongEntry> songMap;
	String messageString;
	String peerlist;
	
	public PeerListHelperThread(Socket cliSocket,ConcurrentHashMap<String,SongEntry> sMap){
		clientSocket = cliSocket;
		songMap = sMap;
	}
	
	public void run() {
		
		messageString = readFromSocket(clientSocket);
		
		peerlist = parseAndPeer(messageString);
		
		writeResponseToSocket(clientSocket, peerlist);
		
		closeSocket(clientSocket);
		
	}
	
	@SuppressWarnings({ "unchecked" })
	public JSONArray getIPList(String key){
		JSONArray list = new JSONArray();
		
		SongEntry sE = songMap.get(key);
		
		for(String ip : sE.ipList){
			JSONObject obj = new JSONObject();
			obj.put(Constants.jsonIPTag, ip);
			list.add(obj);
		}
		
		return list;
	}
	
	public String readFromSocket(Socket clientSocket){
		String inputLine = "";
		String messageString = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while ((inputLine = in.readLine()) != null) {
				messageString = messageString + inputLine;
		    }
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return messageString;
	}
	
	public String parseAndPeer(String messageString){
		
		JSONParser jsonParser = new JSONParser();
		JSONArray returnList = null;
		
		try {
			
			JSONObject peerJson = (JSONObject) jsonParser.parse(messageString);
			String key = (String) peerJson.get(Constants.jsonKeyTag);
			returnList = getIPList(key);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return returnList.toString();
	}
	
	public void writeResponseToSocket(Socket clientSocket,String searchResult){
		PrintWriter out = null;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		out.println(searchResult);

		print("Client search results sent.");
	}
	
	public void closeSocket(Socket clientSocket){
		try {
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void print(String str){
		System.out.println(str);
	}
}
