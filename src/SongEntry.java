import java.util.ArrayList;

public class SongEntry {
	
	ArrayList<String> ipList;
	SongEntity song;
	
	public SongEntry(String titleString, String artistString, String ipString){
		ipList = new ArrayList<String>();
		ipList.add(ipString);
		song = new SongEntity(titleString,artistString);
	}
	
	public static void addIpToEntry(SongEntry songEntry,String ip){
		songEntry.ipList.add(ip);
	}
	
	public static boolean containsIP(SongEntry songEntry,String ip){
		return songEntry.ipList.contains(ip);
	}
	
}

class SongEntity {
	String title;
	String artist;
	public SongEntity(String titleString,String artistString){
		title = titleString;
		artist = artistString;
	}
}
