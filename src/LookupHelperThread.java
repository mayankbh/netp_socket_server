import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class LookupHelperThread extends Thread {
	Socket clientSocket;
	ConcurrentHashMap<String,SongEntry> songMap;
	String messageString;
	String searchResult;
	
	public LookupHelperThread(Socket cliSocket,ConcurrentHashMap<String,SongEntry> sMap){
		clientSocket = cliSocket;
		songMap = sMap;
	}
	
	public void run(){
		
		messageString = readFromSocket(clientSocket);
		
		searchResult = parseAndSearch(messageString);
		
		writeResponseToSocket(clientSocket, searchResult);
		
		closeSocket(clientSocket);
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public JSONArray mapSearch(String searchString){
		
		JSONArray list = new JSONArray();
		Iterator itr = songMap.entrySet().iterator();
		
		while(itr.hasNext()){
			Map.Entry pair = (Map.Entry) itr.next();
			SongEntry currSE = (SongEntry) pair.getValue();
			String songTitle = currSE.song.title;
			String artistName = currSE.song.artist;
			if(songTitle.toLowerCase().contains(searchString.toLowerCase())){
				JSONObject obj = new JSONObject();
				obj.put("Title", songTitle);
				obj.put("Artist", artistName);
				obj.put("Key", songTitle.toLowerCase()+"_"+artistName.toLowerCase());
				list.add(obj);
			}
		}
		
		return list;
	}

	public String readFromSocket(Socket clientSocket){
		String inputLine = "";
		String messageString = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while ((inputLine = in.readLine()) != null) {
				messageString = messageString + inputLine;
		    }
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return messageString;
	}
	
	public void writeResponseToSocket(Socket clientSocket,String searchResult){
		PrintWriter out = null;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		out.println(searchResult);

		print("Client search results sent.");
	}
	
	public String parseAndSearch(String messageString){
		JSONParser jsonParser = new JSONParser();
		JSONArray returnList = null;
		
		try {
			
			JSONObject searchJson = (JSONObject) jsonParser.parse(messageString);
			String searchString = (String) searchJson.get(Constants.jsonSearchTag);
			returnList = mapSearch(searchString);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return returnList.toString();
	}
	
	public void closeSocket(Socket clientSocket){
		try {
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void print(String str){
		System.out.println(str);
	}
	
}
