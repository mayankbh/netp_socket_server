import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;


public class PeerListMainThread extends Thread{
	ConcurrentHashMap<String,SongEntry> songMap;	
	boolean execute;
	public PeerListMainThread(ConcurrentHashMap<String,SongEntry> sMap){
		this.songMap = sMap;
	}
	
	@SuppressWarnings("resource")
	public void run(){
		
		try {
			execute = true;
			ServerSocket peerListSocket = new ServerSocket(Constants.peerListPort);
			print("Listening on peer listing port...");
			while(execute){
				Socket cliSocket = peerListSocket.accept();
				print("Client wants to know list of peers for a song...");
				Thread helper = new PeerListHelperThread(cliSocket,songMap);
				helper.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void stopExecuting() {
        this.execute = false;
    }
	
	public static void print(String str){
		System.out.println(str);
	}
}
