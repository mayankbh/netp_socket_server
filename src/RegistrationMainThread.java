import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;


public class RegistrationMainThread extends Thread {	
	
	ConcurrentHashMap<String,SongEntry> songMap;	
	boolean execute;
	public RegistrationMainThread(ConcurrentHashMap<String,SongEntry> sMap){
		this.songMap = sMap;
	}
	
	@SuppressWarnings("resource")
	public void run(){
		
		try {
			execute = true;
			ServerSocket registrationSocket = new ServerSocket(Constants.registrationPort);
			print("Listening on registration port...");
			while(execute){
				Socket cliSocket = registrationSocket.accept();
				print("New client attempting to register...");
				Thread helper = new RegistrationHelperThread(cliSocket, songMap);
				helper.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void stopExecuting() {
        this.execute = false;
    }
	
	public static void print(String str){
		System.out.println(str);
	}
	
}
