
public class Constants {
	static final String jsonArtistTag = "Artist";
	static final String jsonTitleTag = "Title";
	static final String jsonIPTag = "IP";
	static final String jsonResponseTag = "Response";
	static final String jsonSearchTag = "Title";
	static final String jsonKeyTag = "Key";
	
	static final int registrationPort = 9090;
	static final int lookUpPort = 9091;
	static final int peerListPort = 9092;
	
}
