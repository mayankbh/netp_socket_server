import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class Server {

	static ConcurrentHashMap<String,SongEntry> songMap;
	
	public static void main(String[] args) {
		
		songMap = new ConcurrentHashMap<String,SongEntry>();
		
		Thread registrar = new RegistrationMainThread(songMap);
		Thread lookerUpper = new LookupMainThread(songMap);
		Thread peererIntoTheDark = new PeerListMainThread(songMap);
		
		registrar.start();
		lookerUpper.start();
		peererIntoTheDark.start();
		
		Scanner in = new Scanner(System.in);
		
		//Wait for console input
		// exit -> terminate program
		// print -> print contents of hashmap
		// flush -> clear the hashmap
		while(true){
			String command = in.next();
			if(command.matches("exit")){
				in.close();
				System.exit(0);
			} else if(command.matches("print")){
				print(songMap.toString());
			} else if(command.matches("flush")){
				songMap = new ConcurrentHashMap<String, SongEntry>();
			}
		}
	}
	
	public static void print(String str){
		System.out.println(str);
	}

}
