import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;


public class LookupMainThread extends Thread{
	ConcurrentHashMap<String,SongEntry> songMap;	
	boolean execute;
	public LookupMainThread(ConcurrentHashMap<String,SongEntry> sMap){
		this.songMap = sMap;
	}
	
	@SuppressWarnings("resource")
	public void run(){
		
		try {
			execute = true;
			ServerSocket lookupSocket = new ServerSocket(Constants.lookUpPort);
			print("Listening on lookup port...");
			while(execute){
				Socket cliSocket = lookupSocket.accept();
				print("Client wants to search for a song...");
				Thread helper = new LookupHelperThread(cliSocket, songMap);
				helper.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void stopExecuting() {
        this.execute = false;
    }
	
	public static void print(String str){
		System.out.println(str);
	}
}
